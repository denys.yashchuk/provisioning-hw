package com.voverc.provisioning.service;

import com.voverc.provisioning.entity.Device;
import com.voverc.provisioning.exception.DeviceNotFoundException;
import com.voverc.provisioning.repository.DeviceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class DeviceServiceTest {

    @TestConfiguration
    static class DeviceServiceTestContextConfiguration {

        @Bean
        public DeviceService deviceService() {
            return new DeviceServiceImpl();
        }
    }

    @Autowired
    private DeviceService deviceService;

    @MockBean
    private DeviceRepository deviceRepository;

    @Before
    public void setUp() {
        Device device = new Device();

        device.setMacAddress("a2-b2-c2-d2-e2-f2");
        device.setModel(Device.DeviceModel.DESK);
        device.setUsername("test");
        device.setPassword("test");

        Mockito.when(deviceRepository.findById(device.getMacAddress()))
                .thenReturn(Optional.of(device));
    }

    @Test
    public void whenDeviceExists_thenDeviceShouldBeFound() {
        String macAddress = "a2-b2-c2-d2-e2-f2";
        Device found = deviceService.getDevice(macAddress);

        assertThat(found.getMacAddress())
                .isEqualTo(macAddress);
    }

    @Test(expected = DeviceNotFoundException.class)
    public void whenDeviceDoesNotExist_thenExceptionThrown() {
        String macAddress = "a3-b3-c3-d3-e3-f3";
        Device found = deviceService.getDevice(macAddress);
    }

}