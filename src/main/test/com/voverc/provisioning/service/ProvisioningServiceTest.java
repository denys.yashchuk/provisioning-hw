package com.voverc.provisioning.service;

import com.voverc.provisioning.entity.Device;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import static com.voverc.provisioning.util.Constants.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ProvisioningServiceTest {

    @TestConfiguration
    static class DeviceServiceTestContextConfiguration {

        @Bean
        public ProvisioningService provisioningService() {
            return new ProvisioningServiceImpl();
        }
    }

    @Autowired
    private ProvisioningService provisioningService;

    @MockBean
    private Environment env;

    @MockBean
    private DeviceService deviceService;

    @Before
    public void setUp() {
        Device deskDeviceWithoutOverrideFragment = new Device();
        deskDeviceWithoutOverrideFragment.setMacAddress("a2-b2-c2-d2-e2-f2");
        deskDeviceWithoutOverrideFragment.setModel(Device.DeviceModel.DESK);
        deskDeviceWithoutOverrideFragment.setUsername("test");
        deskDeviceWithoutOverrideFragment.setPassword("test");

        Device deskDeviceWithOverrideFragment = new Device();
        deskDeviceWithOverrideFragment.setMacAddress("2a-2b-2c-2d-2e-2f");
        deskDeviceWithOverrideFragment.setModel(Device.DeviceModel.DESK);
        deskDeviceWithOverrideFragment.setUsername("test2");
        deskDeviceWithOverrideFragment.setPassword("test2");
        deskDeviceWithOverrideFragment.setOverrideFragment("domain=sip.anotherdomain.com\nport=5161\ntimeout=10");

        Device conferenceDeviceWithoutOverrideFragment = new Device();
        conferenceDeviceWithoutOverrideFragment.setMacAddress("a3-b3-c3-d3-e3-f3");
        conferenceDeviceWithoutOverrideFragment.setModel(Device.DeviceModel.CONFERENCE);
        conferenceDeviceWithoutOverrideFragment.setUsername("test3");
        conferenceDeviceWithoutOverrideFragment.setPassword("test3");

        Device conferenceDeviceWithOverrideFragment = new Device();
        conferenceDeviceWithOverrideFragment.setMacAddress("3a-3b-3c-3d-3e-3f");
        conferenceDeviceWithOverrideFragment.setModel(Device.DeviceModel.CONFERENCE);
        conferenceDeviceWithOverrideFragment.setUsername("test4");
        conferenceDeviceWithOverrideFragment.setPassword("test4");
        conferenceDeviceWithOverrideFragment.setOverrideFragment("{\"domain\":\"sip.anotherdomain.com\",\"port\":\"5161\",\"timeout\":10}");

        Mockito.when(deviceService.getDevice(deskDeviceWithoutOverrideFragment.getMacAddress()))
                .thenReturn(deskDeviceWithoutOverrideFragment);
        Mockito.when(deviceService.getDevice(deskDeviceWithOverrideFragment.getMacAddress()))
                .thenReturn(deskDeviceWithOverrideFragment);
        Mockito.when(deviceService.getDevice(conferenceDeviceWithoutOverrideFragment.getMacAddress()))
                .thenReturn(conferenceDeviceWithoutOverrideFragment);
        Mockito.when(deviceService.getDevice(conferenceDeviceWithOverrideFragment.getMacAddress()))
                .thenReturn(conferenceDeviceWithOverrideFragment);

        Mockito.when(env.getProperty(PROVISIONING_DOMAIN_PROPERTY))
                .thenReturn("sip.voverc.com");
        Mockito.when(env.getProperty(PROVISIONING_PORT_PROPERTY))
                .thenReturn("5060");
        Mockito.when(env.getProperty(PROVISIONING_CODECS_PROPERTY))
                .thenReturn("G711,G729,OPUS");
    }

    @Test
    public void whenDeskDeviceWithoutOverrideFragmentExists_thenDefaultPropertyFileMustBeReturned() {
        String macAddress = "a2-b2-c2-d2-e2-f2";
        String result = provisioningService.getProvisioningFile(macAddress);

        assertThat(result.substring(34).trim())
                .isEqualTo("codecs=G711,G729,OPUS\r\n" +
                        "port=5060\r\n" +
                        "password=test\r\n" +
                        "domain=sip.voverc.com\r\n" +
                        "username=test");
    }

    @Test
    public void whenDeskDeviceWithOverrideFragmentExists_thenOverriddenPropertyFileMustBeReturned() {
        String macAddress = "2a-2b-2c-2d-2e-2f";
        String result = provisioningService.getProvisioningFile(macAddress);

        assertThat(result.substring(34).trim())
                .isEqualTo("codecs=G711,G729,OPUS\r\n" +
                        "port=5161\r\n" +
                        "password=test2\r\n" +
                        "timeout=10\r\n" +
                        "domain=sip.anotherdomain.com\r\n" +
                        "username=test2");
    }

    @Test
    public void whenConferenceDeviceWithoutOverrideFragmentExists_thenDefaultJsonMustBeReturned() {
        String macAddress = "a3-b3-c3-d3-e3-f3";
        String result = provisioningService.getProvisioningFile(macAddress);

        assertThat(result.trim())
                .isEqualTo("{\r\n" +
                        "  \"password\" : \"test3\",\r\n" +
                        "  \"port\" : \"5060\",\r\n" +
                        "  \"codecs\" : [ \"G711\", \"G729\", \"OPUS\" ],\r\n" +
                        "  \"domain\" : \"sip.voverc.com\",\r\n" +
                        "  \"username\" : \"test3\"\r\n" +
                        "}");
    }

    @Test
    public void whenConferenceDeviceWithOverrideFragmentExists_thenOverriddenJsonMustBeReturned() {
        String macAddress = "3a-3b-3c-3d-3e-3f";
        String result = provisioningService.getProvisioningFile(macAddress);

        assertThat(result.trim())
                .isEqualTo("{\r\n" +
                        "  \"password\" : \"test4\",\r\n" +
                        "  \"port\" : \"5161\",\r\n" +
                        "  \"codecs\" : [ \"G711\", \"G729\", \"OPUS\" ],\r\n" +
                        "  \"domain\" : \"sip.anotherdomain.com\",\r\n" +
                        "  \"timeout\" : 10,\r\n" +
                        "  \"username\" : \"test4\"\r\n" +
                        "}");
    }

}