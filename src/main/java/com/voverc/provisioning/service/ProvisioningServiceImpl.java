package com.voverc.provisioning.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voverc.provisioning.entity.Device;
import com.voverc.provisioning.exception.PropertiesReadWriteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

import static com.voverc.provisioning.entity.Device.DeviceModel.DESK;
import static com.voverc.provisioning.util.Constants.*;

@Service
public class ProvisioningServiceImpl implements ProvisioningService {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private Environment env;

    public String getProvisioningFile(String macAddress) {
        final Device device = deviceService.getDevice(macAddress);
        return device.getModel().equals(DESK) ? getProvisionForDesk(device) : getProvisionForConference(device);
    }

    @NonNull
    private String getProvisionForDesk(@NonNull final Device device) {
        final Properties properties = new Properties();
        try {
            if (device.getOverrideFragment() != null && !device.getOverrideFragment().isEmpty()) {
                final StringReader reader = new StringReader(device.getOverrideFragment());
                properties.load(reader);
            }
            addDefaultValues(properties, device);
            final ByteArrayOutputStream stream = new ByteArrayOutputStream();
            properties.store(stream, "");
            return new String(stream.toByteArray());
        } catch (IOException e) {
            throw new PropertiesReadWriteException();
        }
    }

    @NonNull
    private String getProvisionForConference(@NonNull final Device device) {
        final Properties properties = new Properties();
        try {
            if (device.getOverrideFragment() != null && !device.getOverrideFragment().isEmpty()) {
                final Map<String, Object> propertiesMap = new ObjectMapper().readValue(device.getOverrideFragment(),
                        new TypeReference<Map<String, Object>>() {
                        });
                properties.putAll(propertiesMap);
            }
            addDefaultValues(properties, device);
            properties.put(CODECS_PROPERTY_KEY, Arrays.asList(properties.getProperty(CODECS_PROPERTY_KEY).split(",")));
            final ObjectMapper objectMapper = new ObjectMapper();
            Map<Object, Object> map = new HashMap<>();
            for (Object o : properties.keySet()) {
                map.put(o, properties.get(o));
            }
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
        } catch (JsonProcessingException e) {
            throw new PropertiesReadWriteException();
        }
    }

    private void addDefaultValues(@NonNull Properties properties, @NonNull Device device) {
        if (!properties.containsKey(USERNAME_PROPERTY_KEY)) {
            properties.put(USERNAME_PROPERTY_KEY, device.getUsername());
        }
        if (!properties.containsKey(PASSWORD_PROPERTY_KEY)) {
            properties.put(PASSWORD_PROPERTY_KEY, device.getPassword());
        }
        if (!properties.containsKey(DOMAIN_PROPERTY_KEY)) {
            properties.put(DOMAIN_PROPERTY_KEY, env.getProperty(PROVISIONING_DOMAIN_PROPERTY));
        }
        if (!properties.containsKey(PORT_PROPERTY_KEY)) {
            properties.put(PORT_PROPERTY_KEY, env.getProperty(PROVISIONING_PORT_PROPERTY));
        }
        if (!properties.containsKey(CODECS_PROPERTY_KEY)) {
            properties.put(CODECS_PROPERTY_KEY, env.getProperty(PROVISIONING_CODECS_PROPERTY));
        }
    }

}
