package com.voverc.provisioning.util;

public class Constants {

    public static final String PROVISIONING_DOMAIN_PROPERTY = "provisioning.domain";
    public static final String PROVISIONING_PORT_PROPERTY = "provisioning.port";
    public static final String PROVISIONING_CODECS_PROPERTY = "provisioning.codecs";

    public static final String USERNAME_PROPERTY_KEY = "username";
    public static final String PASSWORD_PROPERTY_KEY = "password";
    public static final String DOMAIN_PROPERTY_KEY = "domain";
    public static final String PORT_PROPERTY_KEY = "port";
    public static final String CODECS_PROPERTY_KEY = "codecs";

}
