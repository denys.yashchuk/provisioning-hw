package com.voverc.provisioning.controller;

import com.voverc.provisioning.exception.DeviceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorController {

    @ExceptionHandler(DeviceNotFoundException.class)
    public ResponseEntity sendServerError(DeviceNotFoundException ex) {
        return new ResponseEntity(HttpStatus.FORBIDDEN);
    }

}
