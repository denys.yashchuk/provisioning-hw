package com.voverc.provisioning.exception;

import javax.persistence.EntityNotFoundException;

public class DeviceNotFoundException extends EntityNotFoundException {
}
