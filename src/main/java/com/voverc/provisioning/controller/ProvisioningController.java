package com.voverc.provisioning.controller;

import com.voverc.provisioning.service.ProvisioningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class ProvisioningController {

    @Autowired
    private ProvisioningService provisioningService;

    @GetMapping("/provisioning/{macAddress}")
    public ResponseEntity<String> getProvisioningFile(@PathVariable(name = "macAddress") @NonNull final String macAddress){
        return new ResponseEntity<>(provisioningService.getProvisioningFile(macAddress), HttpStatus.OK);
    }

}