package com.voverc.provisioning.service;

import com.voverc.provisioning.entity.Device;
import org.springframework.lang.NonNull;

public interface DeviceService {

    @NonNull
    Device getDevice(@NonNull String macAddress);

}
