package com.voverc.provisioning.service;

import com.voverc.provisioning.entity.Device;
import com.voverc.provisioning.exception.DeviceNotFoundException;
import com.voverc.provisioning.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Override
    @NonNull
    public Device getDevice(@NonNull final String macAddress) {
        return deviceRepository.findById(macAddress).orElseThrow(DeviceNotFoundException::new);
    }

}
