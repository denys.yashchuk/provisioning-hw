package com.voverc.provisioning.repository;

import com.voverc.provisioning.entity.Device;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DeviceRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DeviceRepository deviceRepository;

    @Test
    public void whenFindByMacAddress_thenReturnDevice() {
        Device device = new Device();
        device.setMacAddress("a2-b2-c2-d2-e2-f2");
        device.setModel(Device.DeviceModel.DESK);
        device.setUsername("test");
        device.setPassword("test");
        entityManager.persist(device);
        entityManager.flush();

        Device found = deviceRepository.findById(device.getMacAddress()).get();

        assertThat(found.getMacAddress())
                .isEqualTo(device.getMacAddress());
    }

}
